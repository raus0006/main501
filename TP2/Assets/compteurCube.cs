using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class compteurCube : MonoBehaviour
{
    private int cmpCharge = 0; // compteur de charge
    
    //Fonction appelée automatiquement lorsqu'un autre objet entre dans le déclencheur (Trigger)
    private void OnTriggerEnter(Collider other) 
    {
        // Vérifie si l'objet qui entre a le tag "Charge"
        if (other.CompareTag("Charge"))
        {
            cmpCharge++; // Incrémente le compteur 
            AfficherNombreDeCharges(); // Affiche le nombre de charge dans la zone
        }
    }

     //Fonction appelée automatiquement lorsqu'un autre objet sort du déclencheur (Trigger)

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Charge"))
        {
            cmpCharge--;// Décrémente le compteur 
            AfficherNombreDeCharges(); //Affiche le nombre de charge dans la zone
        }
    }

    // Affiche le nombre de charges dans la zone de dechargement
    private void AfficherNombreDeCharges()
    {
        Debug.Log("Nombre de charges dans la zone de déchargement : " + cmpCharge);
    }

    // Start is called before the first frame update
    void Start()
    {
        // Initialisation du compteur de charges
        cmpCharge = 0;
        AfficherNombreDeCharges();
    }
}




    

