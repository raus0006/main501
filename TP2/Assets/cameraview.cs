using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraview : MonoBehaviour
{
    Camera ext; // camera vue exterieur
    Camera Cabine;// camera vue de la cabine
    Camera Chariot;// camera vue depuis le chariot
    Camera Moufle;// camera vue depuis la moufle

    public Camera[] cameras; //tableau de camera
    private int currentCameraIndex = 0; //camera selectionné

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.C)){ 
            Debug.Log("Camera View"); //affiche dans les logs qu'on change de camera
            SwitchCamera(); //lorsque C est cliqué on appel la fonction SwitchCamera();
        }

        
    }
    //fonction qui permet de changer de camera
    void SwitchCamera()
    {
        cameras[currentCameraIndex].gameObject.SetActive(false); //désactive la caméra actuelle
        if(currentCameraIndex<((cameras.Length)-1)){ // si l'indice de la caméra est plus petite que la taille du tableau
            currentCameraIndex +=1; // on passse à la suivante
        }
        else{
            currentCameraIndex = 0; //sinon on repart du début du tableau
        }
        cameras[currentCameraIndex].gameObject.SetActive(true); // on active la caméra séléctionné
    }
}
