using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouvement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.forward * 0.01f);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.back * 0.01f);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.up, -2);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.up, 2);
        }
        if(Input.GetKey(KeyCode.Space)){ //lorsque l'on clique sur espace
            transform.Translate(Vector3.up * 0.01f); // on déplace notre objet vers le haut à la distance 0.01
        } // On effectue alors un saut
        if(Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.UpArrow) ){ //lorsqu'on clique sur Shift ET fleche du haut
            transform.Translate(Vector3.forward * 0.02f); //on déplace notre objet vers l'avant de 0.02 soit 2fois plus vite que si on cliquait seulement sur la flèche du haut
        } //notre personnage accélère vers l'avant
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.DownArrow)){//lorsqu'on clique sur Shift ET fleche du bas
            transform.Translate(Vector3.back * 0.02f);//on déplace notre objet vers l'arrière de 0.02 soit 2fois plus vite que si on cliquait seulement sur la flèche du bas
        } //notre personnage accélère vers l'arrière
        

    }
}
