using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class poursuite : MonoBehaviour
{
    public float vitesse = 0.005f; //on definit la vitesse
    public GameObject Cible; //on designe une cible

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 positionBot = this.transform.position; //on recupère la position de notre bot
        
        Vector3 positionJoueur = Cible.transform.position; //on recupère la position du joueur (notre cible)
        
        //On applique la formule
        Vector3 direction = (positionJoueur - positionBot).normalized; //on definit la direction: on calcule le vecteur qui pointe du bot vers le joueur qu'on normalise
        
        transform.position += direction * vitesse ; //on déplace notre bot en fonction de la direction calculé auparavant en fonction de la position du joueur et de la vitesse définit auparavant

        
    }
}
