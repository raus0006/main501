using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class deplacerRoue : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow)) //avancer
        {
            transform.Translate(Vector3.up * 0.005f);
        }
        if (Input.GetKey(KeyCode.DownArrow)) //reculer
        {
            transform.Translate(Vector3.down * 0.005f);
        }
        if (Input.GetKey(KeyCode.LeftArrow)) // tourner à gauche
        {
            transform.Rotate(Vector3.forward, -1);
        }
        if (Input.GetKey(KeyCode.RightArrow)) // tourner à droite
        {
            transform.Rotate(Vector3.forward, 1);
        }
        
    }
}
