using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandeMouffle : MonoBehaviour
{
    public float vitesseMoufle = 0.5f; // Vitesse cible pour la montée/descente de la moufle
    public float maxDistance = 5.0f; // Distance maximale de la moufle depuis sa position initiale
    public ArticulationBody moufle;

    private Vector3 initialPosition;

    void Start()
    {
        if (moufle == null)
        {
            Debug.LogError("Assigner l'Articulation Body à la variable 'moufle' dans l'inspecteur.");
            return;
        }

        initialPosition = moufle.transform.position;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            MonterMoufle();
        }
        else if (Input.GetKey(KeyCode.LeftControl))
        {
            DescendreMoufle();
        }
        else
        {
            // Si aucune touche n'est pressée, on arrête le mouvement de la moufle
            StopMoufle();
        }
    }

    void MonterMoufle()
    {
        Vector3 currentPosition = moufle.transform.position;

        if (Vector3.Distance(currentPosition, initialPosition) < maxDistance)
        {
            SetTargetVelocity(vitesseMoufle);
        }
        else
        {
            Debug.Log("Moufle a atteint la distance maximale.");
            StopMoufle();
        }
    }

    void DescendreMoufle()
    {
        Vector3 currentPosition = moufle.transform.position;

        if (Vector3.Distance(currentPosition, initialPosition) > 0)
        {
            SetTargetVelocity(-vitesseMoufle);
        }
        else
        {
            Debug.Log("Moufle a atteint la position minimale.");
            StopMoufle();
        }
    }

    void StopMoufle()
    {
        SetTargetVelocity(0f);
    }

    void SetTargetVelocity(float targetVelocity)
    {
        var drive = moufle.zDrive; 
        drive.targetVelocity = targetVelocity;
        moufle.zDrive = drive;
    }
}
