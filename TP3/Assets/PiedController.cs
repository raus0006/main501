using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PiedController : MonoBehaviour
{
    // Pieds arrières droit et gauche
    public GameObject support_arr_dr_part1, support_arr_dr_part2, support_arr_dr_part3;
    public GameObject support_arr_ga_part1, support_arr_ga_part2, support_arr_ga_part3;
    
    // Pieds avant droit et gauche
    public GameObject support_av_dr_part1, support_av_dr_part2, support_av_dr_part3;
    public GameObject support_av_ga_part1, support_av_ga_part2, support_av_ga_part3;

    // angle pour de rotaion le déploiement
    public float angleDeployPart1_X = 130f;  // Angle X pour la première partie
    public float angleDeployPart1_Y = 50f; // Angle Y pour la première partie
    public float angleDeployPart2 = 100f;  // Angle pour la seconde partie
    public float angleDeployPart3 = 120f;  // Angle pour la troisième partie

    // angle de rotation pour le repli
    public float angleRetractPart1_X = 0f;// Angle X pour la première partie
    public float angleRetractPart1_Y = 0f; // Angle Y pour la première partie
    public float angleRetractPart2 = 0f; // Angle pour la seconde partie
    public float angleRetractPart3 = 0f;// Angle pour la troisième partie

    // ArticulationBody pour chaque partie des pieds
    private ArticulationBody[] allArticulations;

    
    public float delai = 2f; // Temps entre chaque étape de déploiement
    private float timer = 0f;  // Compteur pour suivre le temps écoulé

    private int deploiementEtat = 0;  // Indicateur de l'étape de déploiement
    private bool estDeployer= false;
    private bool estRetracter= false;


    void Start()
    {
        // Initialiser les articulations pour chaque partie des pieds
        allArticulations = new ArticulationBody[]
        {
            support_arr_dr_part1.GetComponent<ArticulationBody>(), support_arr_dr_part2.GetComponent<ArticulationBody>(), support_arr_dr_part3.GetComponent<ArticulationBody>(),
            support_arr_ga_part1.GetComponent<ArticulationBody>(), support_arr_ga_part2.GetComponent<ArticulationBody>(), support_arr_ga_part3.GetComponent<ArticulationBody>(),
            support_av_dr_part1.GetComponent<ArticulationBody>(), support_av_dr_part2.GetComponent<ArticulationBody>(), support_av_dr_part3.GetComponent<ArticulationBody>(),
            support_av_ga_part1.GetComponent<ArticulationBody>(), support_av_ga_part2.GetComponent<ArticulationBody>(), support_av_ga_part3.GetComponent<ArticulationBody>()
        };

        foreach (var articulation in allArticulations)
        {
            SetTargetRotation(articulation, 0f, 0f); // S'assurer que toutes les articulations commencent à une position de repos
        }
    }

    void Update()
    {
        // Touche pour déployer (D)
        // Commence le déploiement
        if (Input.GetKeyDown(KeyCode.D) && !estDeployer && !estRetracter)
        {
            estDeployer = true;
            deploiementEtat = 0;
            timer = 0f;
        }
        
        // Touche pour replier (R)
         // Commence le repli
        if (Input.GetKeyDown(KeyCode.R) && !estDeployer && !estRetracter)
        {
            estRetracter = true;
            deploiementEtat = 0;
            timer = 0f;
        }

        if (estDeployer)
        {
            Deploie();
        }
        else if (estRetracter)
        {
            Retracte();
        }
    }

    void Deploie()
    {
        timer += Time.deltaTime;
        
        if (timer >= delai)
        {
            timer = 0f;  // Réinitialise le timer pour la prochaine étape

            switch (deploiementEtat)
            {
                case 0:
                    SetTargetRotation(allArticulations[0], (-angleDeployPart1_X ), (-angleDeployPart1_Y));// support_arr_dr_part1
                    SetTargetRotation(allArticulations[3], angleDeployPart1_X , angleDeployPart1_Y);// support_arr_ga_part1
                    SetTargetRotation(allArticulations[6], angleDeployPart1_X , (-angleDeployPart1_Y));// support_av_dr_part1
                    SetTargetRotation(allArticulations[9], (-angleDeployPart1_X), angleDeployPart1_Y); // support_av_ga_part1
                    break;
                case 1:
                    SetTargetRotation(allArticulations[1], (-angleDeployPart2), 0f);// support_arr_dr_part2
                    SetTargetRotation(allArticulations[4], (-angleDeployPart2), 0f);// support_arr_ga_part2
                    SetTargetRotation(allArticulations[7], angleDeployPart2, 0f);// support_av_dr_part2
                    SetTargetRotation(allArticulations[10], angleDeployPart2, 0f);// support_av_ga_part2
                    break;
                case 2:
                    SetTargetRotation(allArticulations[2], angleDeployPart3, 0f);// support_arr_dr_part3
                    SetTargetRotation(allArticulations[5], angleDeployPart3, 0f);// support_arr_ga_part3
                    SetTargetRotation(allArticulations[8], (-angleDeployPart3), 0f);// support_av_dr_part3
                    SetTargetRotation(allArticulations[11], (-angleDeployPart3), 0f);// support_av_ga_part3
                    estDeployer = false; // Fin du déploiement
                    break;
            }
            deploiementEtat++;
        }
    }

    void Retracte()
    {
        timer += Time.deltaTime;
        
        if (timer >= delai)
        {
            timer = 0f;

            switch (deploiementEtat)
            {
                case 0:
                    SetTargetRotation(allArticulations[0], angleRetractPart1_X, angleRetractPart1_Y);// support_arr_dr_part1
                    SetTargetRotation(allArticulations[3], angleRetractPart1_X, angleRetractPart1_Y);// support_arr_ga_part1
                    SetTargetRotation(allArticulations[6], angleRetractPart1_X, angleRetractPart1_Y);// support_av_dr_part1
                    SetTargetRotation(allArticulations[9], angleRetractPart1_X, angleRetractPart1_Y);// support_av_ga_part1
                    break;
                case 1:
                    SetTargetRotation(allArticulations[1], angleRetractPart2, 0f);// support_arr_dr_part2
                    SetTargetRotation(allArticulations[4], angleRetractPart2, 0f);// support_arr_ga_part2
                    SetTargetRotation(allArticulations[7], angleRetractPart2, 0f);// support_av_dr_part2
                    SetTargetRotation(allArticulations[10], angleRetractPart2, 0f);// support_av_ga_part2
                    break;
                case 2:
                    SetTargetRotation(allArticulations[2], angleRetractPart3, 0f);// support_arr_dr_part3
                    SetTargetRotation(allArticulations[5], angleRetractPart3, 0f);// support_arr_ga_part3
                    SetTargetRotation(allArticulations[8], angleRetractPart3, 0f);// support_av_dr_part3
                    SetTargetRotation(allArticulations[11], angleRetractPart3, 0f);// support_av_ga_part3
                    estRetracter = false; // Fin du repli
                    break;
            }
            
            deploiementEtat++;
        }
    }
        

    void SetTargetRotation(ArticulationBody articulation, float targetRotationX, float targetRotationY)
    {
        var xDrive = articulation.xDrive;
        xDrive.target = targetRotationX;
        articulation.xDrive = xDrive;

        var yDrive = articulation.yDrive;
        yDrive.target = targetRotationY;
        articulation.yDrive = yDrive;
    }
}
